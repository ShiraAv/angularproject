import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';//function that delays loading of data

@Injectable()
export class UsersService {
 // private _url='http://jsonplaceholder.typicode.com/users
 private _url ='http://shirana.myweb.jce.ac.il/api/users';
usersObservable;
  getUsers (){
    this.usersObservable = this.af.database.list('/users');
    return this.usersObservable;
//return this._http.get(this._url).map(res => res.json()).delay(2000)
  }
  getUsersFromApi(){
return this._http.get(this._url).map(res => res.json());//get url of users from url http/menachemtv.myweb.jce.ac.il/api/users, and turn into json
  }
    adduser(user){
    this.usersObservable.push(user);//push is a function on array, adds object in end of array.
}
 updateuser(user){
let userKey = user.$key;
let userData = {name:user.name,email:user.email};
this.af.database.object('/users/' + userKey).update(userData);
}
  deleteuser(user){
let userKey = user.$key;
this.af.database.object('/users/' + userKey).remove();
    
  }
  constructor(private af:AngularFire, private _http:Http) { }

}
