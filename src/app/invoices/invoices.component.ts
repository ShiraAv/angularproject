import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { InvoicesService } from './invoices.service';
import { Invoice } from '../invoice/invoice';

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }`]
})

export class InvoicesComponent implements OnInit {

  isLoading:Boolean = true;

  invoices;
  invoice:Invoice;
  currentInvoice;

  constructor(private _invoicesService:InvoicesService) { }

  addInvoice(invoice){
  this._invoicesService.addInvoice(invoice);  
}

  select(invoice){
    this.currentInvoice = invoice;
  }

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData => {this.invoices=invoicesData;
    this.isLoading = false;
    console.log(this.invoices)});
  }

}
